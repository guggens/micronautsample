FROM openjdk:11-jdk-slim
COPY build/libs/*-all.jar micronautsample.jar
CMD java ${JAVA_OPTS} -jar micronautsample.jar