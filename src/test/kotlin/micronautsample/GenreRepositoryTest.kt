package micronautsample

import io.micronaut.test.annotation.MicronautTest
import micronautsample.domain.GenreRepository
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import javax.inject.Inject

@MicronautTest
class GenreRepositoryTest {

    @Inject
    lateinit var genreRepository: GenreRepository

    @Inject
    lateinit var config: ApplicationConfiguration


    @Test
    fun verifyConfigurationWorks() {
        assertEquals(28, config.max)
    }


    @Test
    fun listAll() {

        genreRepository.save("test")

        genreRepository.findAll(SortingAndOrderArguments())

    }

}