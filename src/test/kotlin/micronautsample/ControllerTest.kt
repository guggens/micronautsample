package micronautsample


import io.micronaut.http.HttpRequest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test

class ControllerTest {

    @Test
    fun hello() {
        val request = HttpRequest.GET<String>("/hack")
        val body = client.toBlocking().retrieve(request)
        assertNotNull(body)
        assertEquals("hacked!", body)
    }
}