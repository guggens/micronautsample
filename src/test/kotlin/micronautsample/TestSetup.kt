package micronautsample

import io.micronaut.context.ApplicationContext
import io.micronaut.http.client.HttpClient
import io.micronaut.runtime.server.EmbeddedServer

private val server: EmbeddedServer = ApplicationContext.run(EmbeddedServer::class.java)
val client: HttpClient = server.applicationContext.createBean(HttpClient::class.java, server.url)
