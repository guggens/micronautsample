package micronautsample

import io.micronaut.http.HttpRequest
import io.micronaut.http.HttpStatus
import micronautsample.domain.Genre
import micronautsample.domain.GenreSaveCommand
import micronautsample.domain.GenreUpdateCommand
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.fail
import org.junit.jupiter.api.Test

class GenreControllerTest {

    @Test
    fun saveGenre() {
        val request = HttpRequest.POST<GenreSaveCommand>("/genres/", GenreSaveCommand("horror"))
        val response = client.toBlocking().exchange(request, Genre::class.java)

        assertEquals(HttpStatus.CREATED, response.status)

        response.body() ?: fail("body expected.")
        assertEquals("horror", response.body()!!.name)
    }

    @Test
    fun updateGenre() {
        val request = HttpRequest.POST<GenreSaveCommand>("/genres/", GenreSaveCommand("horror"))
        val createResponse = client.toBlocking().exchange(request, Genre::class.java)

        val createdId = createResponse.body()!!.id!!

        val updateRequest = HttpRequest.PUT<GenreUpdateCommand>("/genres/", GenreUpdateCommand(createdId, "Horror"))
        client.toBlocking().exchange(updateRequest, Any::class.java)

        val getRequest = HttpRequest.GET<Genre>("/genres/$createdId")
        val response = client.toBlocking().exchange(getRequest, Genre::class.java)
        assertEquals("Horror", response.body.get().name)

    }

}