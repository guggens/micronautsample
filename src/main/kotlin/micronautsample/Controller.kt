package micronautsample

import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get

@Controller("/hack")
class Controller {

    @Get
    fun hackme(): String {
        return "hacked!"
    }


}