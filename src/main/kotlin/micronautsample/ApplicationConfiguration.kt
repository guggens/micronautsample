package micronautsample

import io.micronaut.context.annotation.ConfigurationProperties

@ConfigurationProperties("application")
class ApplicationConfiguration {
    var max: Int = 10
}
