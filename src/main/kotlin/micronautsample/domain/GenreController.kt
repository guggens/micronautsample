package micronautsample.domain

import io.micronaut.http.HttpHeaders
import io.micronaut.http.HttpResponse
import io.micronaut.http.annotation.*
import io.micronaut.validation.Validated
import micronautsample.SortingAndOrderArguments
import java.net.URI
import javax.inject.Singleton
import javax.validation.Valid

@Validated
@Controller("/genres")
@Singleton
class GenreController(protected val genreRepository: GenreRepository)// <3>
{

    @Get("/{id}") // <4>
    fun show(id: Long?): Genre {
        return genreRepository
                .findById(id)
                .orElse(null) // <5>
    }

    @Put("/") // <6>
    fun update(@Body @Valid command: GenreUpdateCommand): HttpResponse<*> { // <7>
        val numberOfEntitiesUpdated = genreRepository.update(command.id, command.name!!)

        return HttpResponse
                .noContent<Any>()
                .header(HttpHeaders.LOCATION, location(command.id).path) // <8>
    }

    @Get(value = "/list{?args*}") // <9>
    fun list(@Valid args: SortingAndOrderArguments): List<Genre> {
        return genreRepository.findAll(args)
    }

    @Post("/") // <10>
    fun save(@Body @Valid cmd: GenreSaveCommand): HttpResponse<Genre> {
        val genre = genreRepository.save(cmd.name!!)

        return HttpResponse
                .created(genre)
                .headers { headers -> headers.location(location(genre.id)) }
    }

    @Delete("/{id}") // <11>
    fun delete(id: Long?): HttpResponse<*> {
        genreRepository.deleteById(id)
        return HttpResponse.noContent<Any>()
    }

    protected fun location(id: Long?): URI {
        return URI.create("/genres/" + id!!)
    }

    protected fun location(genre: Genre): URI {
        return location(genre.id)
    }
}
