package micronautsample.domain

import io.micronaut.configuration.hibernate.jpa.scope.CurrentSession
import io.micronaut.spring.tx.annotation.Transactional
import micronautsample.ApplicationConfiguration
import micronautsample.SortingAndOrderArguments
import java.util.*
import javax.inject.Singleton
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull

private val VALID_PROPERTY_NAMES = Arrays.asList("id", "name")

@Singleton
class GenreRepository(@param:CurrentSession @field:PersistenceContext
                      private val entityManager: EntityManager,
                      private val applicationConfiguration: ApplicationConfiguration
)
{

    @Transactional(readOnly = true)
    fun findById(@NotNull id: Long?): Optional<Genre> {
        return Optional.ofNullable(entityManager.find(Genre::class.java, id))
    }

    @Transactional // <4>
    fun save(@NotBlank name: String): Genre {
        val genre = Genre(name = name)
        entityManager.persist(genre)
        return genre
    }

    @Transactional
    fun deleteById(@NotNull id: Long?) {
        findById(id).ifPresent { genre -> entityManager.remove(genre) }
    }

    @Transactional(readOnly = true)
    fun findAll(@NotNull args: SortingAndOrderArguments): List<Genre> {
        var qlString = "SELECT g FROM Genre as g"
        if (args.order != null && args.sort != null && VALID_PROPERTY_NAMES.contains(args.sort)) {
            qlString += " ORDER BY g." + args.sort + " " + args.order.toLowerCase()
        }
        val query = entityManager.createQuery(qlString, Genre::class.java)
        query.maxResults = args.max ?: applicationConfiguration.max
        query.firstResult = args.offset

        return query.resultList
    }

    @Transactional
    fun update(@NotNull id: Long?, @NotBlank name: String): Int {
        return entityManager.createQuery("UPDATE Genre g SET name = :name where id = :id")
                .setParameter("name", name)
                .setParameter("id", id)
                .executeUpdate()
    }
}
