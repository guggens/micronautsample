package micronautsample.domain

import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull

class GenreUpdateCommand(
        @NotNull
        var id: Long? = null,

        @NotBlank
        var name: String? = null
)