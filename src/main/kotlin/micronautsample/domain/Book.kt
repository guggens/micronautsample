package micronautsample.domain

import javax.persistence.*
import javax.validation.constraints.NotNull

@Entity
data class Book
(
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        var id: Long? = null,

        @NotNull
        @Column(name = "name", nullable = false)
        var name: String? = null,

        @NotNull
        @Column(name = "isbn", nullable = false)
        var isbn: String? = null,

        @ManyToOne
        var genre: Genre? = null
)
