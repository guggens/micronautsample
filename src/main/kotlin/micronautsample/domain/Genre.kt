package micronautsample.domain

import com.fasterxml.jackson.annotation.JsonIgnore
import java.util.*
import javax.persistence.*
import javax.validation.constraints.NotNull

@Entity
data class Genre(

        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        var id: Long? = null,

        @NotNull
        @Column(name = "name", nullable = false, unique = true)
        var name: String? = null,

        @JsonIgnore
        @OneToMany(mappedBy = "genre")
        var books: Set<Book> = HashSet()

)
