package micronautsample.domain

import javax.validation.constraints.NotBlank

data class GenreSaveCommand(
        @NotBlank
        var name: String? = null
)
