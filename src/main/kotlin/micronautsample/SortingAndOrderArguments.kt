package micronautsample

import javax.validation.constraints.Pattern
import javax.validation.constraints.Positive
import javax.validation.constraints.PositiveOrZero

data class SortingAndOrderArguments (
    @PositiveOrZero
    val offset: Int = 0,

    @Positive // <1>
    val max: Int? = null,

    @Pattern(regexp = "id|name")  // <1>
    val sort: String? = null,

    @Pattern(regexp = "asc|ASC|desc|DESC")  // <1>
    val order: String? = null
)
