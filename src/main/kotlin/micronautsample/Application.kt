package micronautsample

import io.micronaut.runtime.Micronaut

fun main() {
    Micronaut.build()
            .packages("micronautsample")
            .start()
}